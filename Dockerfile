# Build a Debian
FROM debian:jessie

LABEL Author="Souleymane BA <ba.souleymane313@gmail.com>" @Copyright="BeautyOnce"

# Non interactive mode for mysql install for example
ENV DEBIAN_FRONTEND=noninteractive

# Update the repository sources list
RUN apt-get update


# Install basics
RUN apt-get update && apt-get install -y \
apt-utils \
curl \
wget

# Add dotdeb
RUN echo "deb http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
RUN echo "deb-src http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list
RUN wget https://www.dotdeb.org/dotdeb.gpg && apt-key add dotdeb.gpg && rm dotdeb.gpg

# Add nodesource
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash -


# Add repository for php7.1
RUN apt-get install apt-transport-https lsb-release ca-certificates && \
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list && \
apt-get update

# Install packages
RUN apt-get update && apt-get install -y \
zsh \
git \
vim \
htop \
vim \
htop \
mysql-server \
redis-server \
supervisor \
openssh-server \
apache2 \
apache2-bin \
apache2-data \
apache2-utils \
php7.1 \
php7.1-xml \
php7.1-mbstring \
php7.1-mysql \
php7.1-json \
php7.1-curl \
php7.1-cli \
php7.1-common \
php7.1-mcrypt \
php7.1-gd \
libapache2-mod-php7.1 \
php7.1-zip \
telnet \
unzip \
nodejs


# Run oh my zsh
RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true

# Configure System
ADD config/system/rc.local /etc/rc.local
ADD config/system/motd /etc/motd

# Configure Supervisord
ADD config/supervisor/app.conf /etc/supervisor/conf.d/app.conf

# Configure SSH Server
RUN sed -ie 's/PermitRootLogin without-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

# Set root password
RUN echo "root:root" | chpasswd
RUN chsh -s /bin/zsh root

# Configure Timezone
RUN echo "" >> /etc/profile
RUN echo "TZ='Europe/Paris'; export TZ" >> /etc/profile

RUN echo "" >> /etc/zsh/zshrc
RUN echo "TZ='Europe/Paris'; export TZ" >> /etc/zsh/zshrc
RUN echo "" >> /etc/zsh/zshrc
RUN echo "myip() { ip addr show | grep inet | grep eth0 | awk '{print \$2}' | cut -d'/' -f1 }" >> /etc/zsh/zshrc
RUN echo "" >> /etc/zsh/zshrc
RUN echo 'alias aa="tail -100f /var/log/apache2/access.log"' >> /etc/zsh/zshrc
RUN echo 'alias ae="tail -100f /var/log/apache2/error.log"' >> /etc/zsh/zshrc
RUN echo 'alias phpunit="vendor/bin/phpunit"' >> /etc/zsh/zshrc
RUN echo 'alias art="php artisan"' >> /etc/zsh/zshrc
RUN echo 'alias xdebug="export XDEBUG_CONFIG=\"idekey=PHPSTORM\""' >> /etc/zsh/zshrc

# Configure MySQL
ADD config/mysql/debian.cfg /etc/mysql/debian.cfg

# Configure Apache
# RUN a2enmod rewrite
# COPY /config/apache/000-default.conf /etc/apache2/sites-available/laravel.conf
# ADD config/apache/app.conf /etc/apache2/sites-available/laravel.conf
# RUN a2ensite laravel.conf
# RUN a2dissite 000-default.conf
# ADD files/app_tools /var/www/html

# Configure PHP
ADD config/php/xdebug.ini /etc/php/7.1/apache2/conf.d/20-xdebug.ini
RUN sed -ie 's/memory_limit\ =\ 128M/memory_limit\ =\ 2G/g' /etc/php/7.1/apache2/php.ini
RUN sed -ie 's/\;date\.timezone\ =/date\.timezone\ =\ Europe\/Paris/g' /etc/php/7.1/apache2/php.ini
RUN sed -ie 's/upload_max_filesize\ =\ 2M/upload_max_filesize\ =\ 200M/g' /etc/php/7.1/apache2/php.ini
RUN sed -ie 's/post_max_size\ =\ 8M/post_max_size\ =\ 200M/g' /etc/php/7.1/apache2/php.ini

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


# Envoy
RUN composer global require "laravel/envoy=~1.0"
RUN echo "" >> /root/.zshrc
RUN echo "export PATH=\$HOME/.composer/vendor/bin:\$HOME/bin:/usr/local/bin:\$PATH" >> /root/.zshrc

# Set correct rights
RUN chown -R www-data:www-data /var/www/html

# Set root password
RUN echo "root:root" | chpasswd
RUN chsh -s /bin/zsh root

# Open ports
EXPOSE 22
EXPOSE 80

# Link Laravel App
#RUN mkdir /app
RUN rm /var/www/html/index.html
ADD php-project/index.php /var/www/html/index.php
WORKDIR /var/www/html

# Configure persistent Volumes
VOLUME ["/var/lib/mysql"]

# Run
RUN mkdir -p /root/script /root/config
ADD entrypoint.sh ./entrypoint.sh
RUN chmod +x ./entrypoint.sh

CMD [ "./entrypoint.sh" ]
