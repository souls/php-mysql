<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </head>
  <body>
    
    <div class="jumbotron text-center">
        <h1>BeautyOnce white app</h1>
        <p>Démarrer votre projet sous un container et déployer quand vous voulez</p> 
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
            <h3>Vérification de la connectivité sur Mysql</h3>
            <?php
                $link = mysqli_connect("localhost", "laravel", "laravel", "laravel");

                if (!$link) {
                    echo "Error: Unable to connect to MySQL." . PHP_EOL;
                    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
                    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
                    exit;
                }

                echo "<p>Success: Connection Mysql succesfull</p>" . PHP_EOL;
                echo "<p>Host information: </p>" . mysqli_get_host_info($link) . PHP_EOL;

                mysqli_close($link);
            ?>
        </div>
        <div class="col-sm-6">
            <h3>Deployer !!!</h3>
            <p>Déployer sur differérent env: Développement, Intégration, recette et production</p>
        </div>
      </div>
  </div>
  </body>
</html>
